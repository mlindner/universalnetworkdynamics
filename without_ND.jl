## Fit coupling of 2nd order Kuramoto model with an ANN without NetworkDynamics

using DiffEqFlux, NetworkDynamics, Graphs, OrdinaryDiffEq, Plots, Random, SciMLSensitivity, Optimization, OptimizationFlux, LinearAlgebra

## Defining the graph

Random.seed!(1)

N = 3 # nodes
k = 2
g = barabasi_albert(N, k) # graph
B = incidence_matrix(g; oriented=true)

transpose(B) .* [1, 2]

struct kuramoto_dyn{T,T2,U}
    B::T
    B_trans::T2
    ω::U
    N::Int
end

# callable struct with differential equation of Kurmaoto-Oscialltor (2-dim)
# x and dx arrays containing 2 variables (x[1:N] : ω , x[N+1:2N] : ϕ)
function (dd::kuramoto_dyn)(dx, x, p, t)
    dx[1:N] .= dd.ω .- x[1:N] .- 5.0 .* dd.B * sin.(dd.B_trans * x[N+1:2N])
    dx[N+1:2N] .= x[1:N]
    nothing
end

# constructing ordered eigenfrequencies of the N vertices
ω = Array{Float64}(1:N) ./ N

kn = kuramoto_dyn(B, transpose(B), ω, N)
# Jv = JacVecOperator(kn, randn(nv(g)), nothing, 0.0)
# callable struct kn is handed over to ODEFunction
kur_network_L = ODEFunction(kn) #, jac_prototype=Jv)

x0_L = 0.1 .* Array{Float64}(1:2N)
dx_L = similar(x0_L)
datasize = 100 # Number of data points
tspan = (0.0, 100.0)
tsteps = range(tspan[1], tspan[2], length=datasize)

kur_network_L(dx_L, x0_L, nothing, 0.0)

### Simulation


prob_L = ODEProblem(kur_network_L, x0_L, tspan, nothing)
sol_L = solve(prob_L, Tsit5(), rtol= 1e-6, saveat=tsteps)
diff_data = Array(sol_L)

ptspan = (90.0, 100.0)
plot(sol_L; vars=collect(1:3), tspan=ptspan)
plot(sol_L; vars=collect(1:3), tspan=(0.0, 10.0))

### Learning
ann_diff_node = FastChain(
  FastDense(2, 30, tanh),
  FastDense(30, 2))

ann_diff_edge = FastChain(
    FastDense(1, 30, tanh),
    FastDense(30, 1))

struct kuramoto_ann{T,T2,U}
    B::T
    B_trans::T2
    ω::U
    N::Int
end

function (dd::kuramoto_ann)(dx, x, p, t)
    #dx[1:N] .= dd.ω .- x[1:N] .- 5.0 .* dd.B * sin.(dd.B_trans * x[N+1:2N])
    dx[1:N] .= dd.ω .- x[1:N] .- 5.0 .* dd.B * map(x -> ann_diff_edge(x, p)[1], dd.B_trans * x[dd.N+1:2dd.N]) # learn only coupling
    #dx[1:N] .= map(x -> ann_diff_node(x, p), dd.ω, x, dd.B) * map(x -> ann_diff_edge(x, p), dd.B_trans * x)
    dx[N+1:2N] .= x[1:N]
    #dx[1:N] .= map(x -> ann_diff_node(x, p)[1],  dd.ω .- x[1:N] .- 5.0 .* dd.B) * sin.(dd.B_trans * x[N+1:2N])
    #dx[N+1:2N] .= map(x -> ann_diff_node(x, p)[2], x[1:N]) # learn only seceond part of node function - funktioniert
    nothing
end


# closer look at dimensionalities
map(x -> ann_diff_node(x, initial_params(ann_diff_node))[2], view(x0_L, 1:N))
map(x -> ann_diff_node(x, initial_params(ann_diff_node))[2], x0_L[1:N])

view(x0_L, 1:1:N)
x0_L[1:N]

map(x -> ann_diff_node(x, initial_params(ann_diff_node)), ω .- x0_L[1:N] .- 5.0 .* B)
ω .- x0_L[1:N] .- 5.0 .* B

ω .- x0_L[1:N] .- 5.0 .* B * map(x -> ann_diff_edge(x, initial_params(ann_diff_edge))[1], transpose(B) * x0_L[N+1:2N])

# closer look fertig

initial_params(ann_diff_node)
initial_params(ann_diff_edge)
kn_ann = kuramoto_ann(B, transpose(B), ω, N)
kur_network_ann = ODEFunction(kn_ann)
kur_network_ann(dx_L, x0_L, initial_params(ann_diff_edge), 0.0)


prob_neuralode = ODEProblem(kur_network_ann, x0_L, tspan, initial_params(ann_diff_edge))
sol_ann = solve(prob_neuralode, Tsit5(); reltol=1e-6, saveat=tsteps)
Array(sol_ann)
plot(sol_ann; vars=collect(1:3), tspan=tspan, color=:red, ylims=[0.25,0.7])
plot!(sol_L; vars=collect(1:3), tspan=tspan, color=:black)

function predict_neuralode(p)
    x = Array(solve(prob_neuralode,Tsit5(),p=p,saveat=tsteps,
    #sensealg=InterpolatingAdjoint(autojacvec=ReverseDiffVJP(true))))
    sensealg=SciMLSensitivity.QuadratureAdjoint(autojacvec=SciMLSensitivity.ReverseDiffVJP(true))))
end  
  
  #function loss_neuralode(p)
  #  pred = predict_neuralode(p)
  #  loss = sum(abs2, diff_data .- pred)
  #  return loss, pred
  #end
  
function loss_neuralode(p)
    x = predict_neuralode(p)
    loss = sum(norm.(x - diff_data))
    return loss
end
  
  
  
  ## Cache solutions for animation
  #sols = [diff_data[2:2:N, :]']
  
iter = 0
function callback(p, l)
    global iter
    iter += 1
    if iter % 1 == 0
      println(l)
    end
    return false
end
p_ann = vcat(initial_params(ann_diff_edge))
  
  
  
adtype = Optimization.AutoZygote()
optf = Optimization.OptimizationFunction((x,p) -> loss_neuralode(x), adtype)
optprob = Optimization.OptimizationProblem(optf, p_ann)
res1 = Optimization.solve(optprob,
                            ADAM(0.01), 
                            callback = callback, 
                            maxiters = 1000)

plot(Array(predict_neuralode(res1.u))', color=:red, ylims=[0.25,0.7])
plot!(Array(sol_L)', color=:black)
  
