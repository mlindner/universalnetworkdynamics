# Fit topology of 2nd order Kuramoto model with an ANN

using DiffEqFlux
using NetworkDynamics
using Graphs
using OrdinaryDiffEq
using Plots
using GalacticOptim
using Random


## Defining the graph

Random.seed!(2)

N = 30 # nodes

g_true = watts_strogatz(N, 4, 0.1)

##

g = complete_graph(N)

# edgebools = shuffle!(rand(Bool,N*(N-1) ÷ 2)) .|> Float32

edgebools = zeros(Float32, ne(g))

true_edges = collect(edges(g_true))

for (i, e) in enumerate(edges(g))
  if e ∈ true_edges
    edgebools[i] = 1.
  end
end

## Network dynamics vertex and edge functions
struct kuramoto_inertia!
  p::Float32
end


function (km::kuramoto_inertia!)(dv, v, edges, p, t)
  dv[1] = v[2]
  dv[2] = km.p - v[2]
  for edge in edges
    dv[2] += edge[1]
  end
  return nothing
end

@inline function diffusion_edge!(e, v_s, v_d, p, t)
  e[1] = p * sin(v_s[1] - v_d[1])
  nothing
end


staticedge = StaticEdge(; f=diffusion_edge!, dim=1)

# generating random values for the parameter value ω_0 of the vertices
v_pars = randn(nv(g))
v_pars .-= sum(v_pars) / nv(g)
odevertices = ODEVertex[]
for p in v_pars
  push!(odevertices, ODEVertex(; f=kuramoto_inertia!(p), dim=2, sym=[:θ, :ω]))
end


kuramoto_network! = network_dynamics(odevertices, staticedge, g)

### Simulation and Plotting

# constructing random initial conditions for nodes (variable θ)
x0 = randn(Float32, 2nv(g))
dx = similar(x0)
datasize = 100 # Number of data points
tspan = (0.0f0, 15.0f0) # Time range
tsteps = range(tspan[1], tspan[2], length=datasize)

diff_prob = ODEProblem(kuramoto_network!, x0, tspan, (nothing, edgebools))
diff_sol = solve(diff_prob, Tsit5();  saveat=tsteps)
diff_data = Array(diff_sol)


plot(diff_sol, vars=2:2:2N)

#### Learning 


ann_network = (dx,x,p,t) -> diff_prob.f(dx,x,(nothing,p),t)

prob_neuralode = ODEProblem(ann_network, x0, tspan, ones(ne(g)))


sol_node = solve(prob_neuralode, Tsit5(), saveat=tsteps)
sol_node[2:2:2N, :] == Array(sol_node)[2:2:2N, :]

plot(Array(sol_node[2:2:2N, :])', color=:red)
plot!(Array(diff_sol[2:2:2N, :])', color=:black)


function predict_neuralode(p)
  tmp_prob = remake(prob_neuralode, p=p)
  Array(solve(tmp_prob, Tsit5(), saveat=tsteps))
end

const λ = 0.0001
one_or_zero(x) = abs(x) * abs(x-1)

function loss_neuralode(p)
  pred = predict_neuralode(p)
  loss = sum(abs2, diff_data .- pred) + λ * sum(one_or_zero, p)
  return loss, pred
end



## Cache solutions for animation
sols = [diff_data[2:2:2N, :]']

callback = function (p, l, pred; doplot=true)
  display(l)
  # plot current prediction against data
   plt = plot(diff_data[2:2:2N, :]', label=["data" ""], color=:black, title=l)
   plot!(plt, pred[2:2:2N, :]', label=["prediction" ""], color=:red)
  push!(sols, pred[2:2:2N, :]')
  if doplot
    display(plot(plt))
  end
  return false
end

loss_neuralode(ones(ne(g)))
callback(initial_params(ann_network), loss_neuralode(ones(ne(g)))...)

##


## This takes very long and produces a lot of strange repl outpude
# It trains though
# Should set some maxiters
result_neuralode = DiffEqFlux.sciml_train(loss_neuralode, prob_neuralode.p, cb=callback, maxiters = 120)
sols
anim = @animate for i in eachindex(sols)
  plt = plot(tsteps, diff_data[2:2:2N, :]', label=["data" ["" for i = 1:N-1]...],
    color=:black, ylim=[-2, 2], title =  "Epoch $(i)")
  plot!(plt, tsteps, sols[i], label=["ann fit" ["" for i = 1:N-1]...], color=:red)
end
gif(anim, "plots/edge_ann.gif", fps=30)

result_neuralode.u
result_neuralode.u ≈ edgebools


scatter(result_neuralode.u)

# Check how well the coupling functions is fit 
# (x,y) = (0,y) ↦ sin(-y) = -sin(y)

points = hcat(zeros(1601), collect(-8.0:0.01:8.0))

ann_points = mapslices(x -> ann_diff(x, result_neuralode.u), points, dims=2)

#plot(points[:, 2], label="x")
plot(collect(-8.0:0.01:8.0), -sin.(points[:, 2]), label="data",
  title="coupling function (x,y) -> sin(x-y) at (0,y)")
plot!(collect(-8.0:0.01:8.0), ann_points[:, 1], label="fit")
png("edge_check.png")

