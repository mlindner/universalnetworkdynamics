## Meta parameter study for the ANN training. 

using DiffEqFlux, NetworkDynamics, Graphs, OrdinaryDiffEq, Plots, Random, SciMLSensitivity, Optimization, OptimizationFlux, LinearAlgebra, DelimitedFiles

v = range(-5,stop=1,length=25)
A = [10 .^i for i in v]


# 2 random seeds
# neural network size: 30, 60
# 25 Meta parameter

# first for loop: random seed
# second for loop: neural network size
# third for loop: meta parameter for ADAM optimizer
number_of_param_arr = [30, 60]

for random_seed in 1:2
    Random.seed!(random_seed)
for number in 2:2
    chainsize = number_of_param_arr[number]



v = range(-5,stop=1,length=25)
A = [10 .^i for i in v]


## Defining the graph

N = 3 # nodes
k = 2 # node degree
g = barabasi_albert(N, k) # graph


## Network dynamics vertex and edge functions
struct kuramoto_inertia!
  p::Float32
end


function (km::kuramoto_inertia!)(dv, v, edges, p, t)
  dv[1] = v[2]
  dv[2] = km.p - v[2]
  for edge in edges
    dv[2] += edge[1]
  end
  return nothing
end

@inline function diffusion_edge!(e, v_s, v_d, p, t)
  e[1] = sin(v_s[1] - v_d[1])
  nothing
end


staticedge = StaticEdge(; f=diffusion_edge!, dim=1)

## generating random values for the parameter value ω_0 of the vertices

v_pars = randn(nv(g))
v_pars .-= sum(v_pars) / nv(g)
odevertices = ODEVertex[]
for p in v_pars
  push!(odevertices, ODEVertex(; f=kuramoto_inertia!(p), dim=2, sym=[:θ, :ω]))
end


kuramoto_network! = network_dynamics(odevertices, staticedge, g)

## Simulation and Plotting

# constructing random initial conditions for nodes (variable θ)
x0 = randn(Float32, 2nv(g))
dx = similar(x0)
datasize = 100 # Number of data points
tspan = (0.0f0, 15.0f0) # Time range
tsteps = range(tspan[1], tspan[2], length=datasize)

diff_prob = ODEProblem(kuramoto_network!, x0, tspan, nothing)
diff_sol = solve(diff_prob, Tsit5(); reltol=1e-6, saveat=tsteps)
diff_data = Array(diff_sol)


#plot(diff_sol, vars=2:2:2N)

## Learning 

ann_diff_node = FastChain(
  FastDense(2, chainsize, tanh),
  FastDense(chainsize, 2))

ann_diff_edge = FastChain(
    FastDense(2, chainsize, tanh),
    FastDense(chainsize, 1, tanh))


# parameter handling

init_param_ann = vcat(initial_params(ann_diff_node), initial_params(ann_diff_edge)) # total: 273, 543
length_node = length(initial_params(ann_diff_node))
length_edge = length(initial_params(ann_diff_edge))
length_param = length(init_param_ann)


struct kuramoto_ann!
  p::Float32
end

@inline function (km::kuramoto_ann!)(dv, v, edges, p, t)
  # Introducing a buffer for the ann_diff argument will save allocations
  #dv .= ann_diff([dv[1], dv[2], v[1], v[2], km.p], p)
  #dv .= ann_diff([v[1], v[2], km.p], p)
  dv .= ann_diff_node([v[2], km.p], view(p, 1:length_node)) # damit klappt es 
  # We know that the coupling is additive
  for edge in edges
    dv[2] += edge[1]
  end
  nothing
end


annvertices = ODEVertex[]
for p in v_pars
  push!(annvertices, ODEVertex(; f=kuramoto_ann!(p), dim=2, sym=[:θ, :ω]))
end


@inline function ann_edge!(e, v_s, v_d, p, t)
  e[1] = ann_diff_edge([v_s[1], v_d[1]], view(p, length_node+1:length_param))[1]
  nothing
end

annedge = StaticEdge(; f=ann_edge!, dim=1, coupling=:antisymmetric)



ann_network = network_dynamics(annvertices, annedge, g)

prob_neuralode = ODEProblem(ann_network, x0, tspan, init_param_ann)

sol_node = solve(prob_neuralode, Tsit5(), saveat=tsteps)
sol_node[2:2:N, :] == Array(sol_node)[2:2:N, :]

#plot(Array(sol_node[2:2:2N, :])', color=:red, ylims=[-2,2])
#plot!(Array(diff_sol[2:2:2N, :])', color=:black)

function predict_neuralode(p)
  x = Array(solve(prob_neuralode,Tsit5(),p=p,saveat=tsteps,
  #sensealg=InterpolatingAdjoint(autojacvec=ReverseDiffVJP(true))))
  sensealg=SciMLSensitivity.QuadratureAdjoint(autojacvec=SciMLSensitivity.ReverseDiffVJP(true))))
end  

function loss_neuralode(p)
  x = predict_neuralode(p)
  loss = sum(norm.(x - diff_data))
  return loss
end



## Cache solutions for animation
#sols = [diff_data[2:2:N, :]']

for i in 6:15
  metaparam = A[i]

  open(string("meta_studies/metaparam_", metaparam, "_number_", chainsize, "_randseed_", random_seed, ".txt"), "w") do io
    write(io, string(metaparam, "\n"))
    write(io, string(chainsize, "\n"))
    write(io, string(random_seed, "\n"))
end;



iter = 0
function callback(p, l)
  global iter
  open(string("meta_studies/metaparam_", metaparam, "_number_", chainsize, "_randseed_", random_seed, ".txt"), "a") do io
    iter += 1
    if iter % 1 == 0
      write(io, string(l, "\n"))
      #println(l)
    end
  end;
  return false
end
p_ann = init_param_ann



adtype = Optimization.AutoZygote()
optf = Optimization.OptimizationFunction((x,p) -> loss_neuralode(x), adtype)
optprob = Optimization.OptimizationProblem(optf, p_ann)
res1 = Optimization.solve(optprob,
                          ADAM(metaparam), 
                          callback = callback, 
                          maxiters = 5000)

end
end
end

# ADAM: 68
#plot(Array(predict_neuralode(res1.u)[2:2:2N, :])', color=:red, ylims=[-2,2])
#plot!(Array(diff_sol[2:21:2N, :])', color=:black)


