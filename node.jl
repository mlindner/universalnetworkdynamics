## Fit node dynamics of 2nd order Kuramoto model with an ANN

using DiffEqFlux, NetworkDynamics, Graphs, OrdinaryDiffEq, Plots
using Random, SciMLSensitivity, Optimization, OptimizationFlux, LinearAlgebra


## Defining the graph

Random.seed!(1)

N = 3 # nodes
k = 2 # node degree
g = barabasi_albert(N, k) # graph


## Network dynamics vertex and edge functions
struct kuramoto_inertia!
  p::Float32
end


function (km::kuramoto_inertia!)(dv, v, edges, p, t)
  dv[1] = v[2]
  dv[2] = km.p - v[2]
  for edge in edges
    dv[2] += edge[1]
  end
  return nothing
end

@inline function diffusion_edge!(e, v_s, v_d, p, t)
  e[1] = sin(v_s[1] - v_d[1])
  nothing
end


staticedge = StaticEdge(; f=diffusion_edge!, dim=1)

## generating random values for the parameter value ω_0 of the vertices

v_pars = randn(nv(g))
v_pars .-= sum(v_pars) / nv(g)
odevertices = ODEVertex[]
for p in v_pars
  push!(odevertices, ODEVertex(; f=kuramoto_inertia!(p), dim=2, sym=[:θ, :ω]))
end


kuramoto_network! = network_dynamics(odevertices, staticedge, g)

## Simulation and Plotting

# constructing random initial conditions for nodes (variable θ)
x0 = randn(Float32, 2nv(g))
dx = similar(x0)
datasize = 100 # Number of data points
tspan = (0.0f0, 15.0f0) # Time range
tsteps = range(tspan[1], tspan[2], length=datasize)

diff_prob = ODEProblem(kuramoto_network!, x0, tspan, nothing)
diff_sol = solve(diff_prob, Tsit5(); reltol=1e-6, saveat=tsteps)
diff_data = Array(diff_sol)


plot(diff_sol, vars=2:2:N)

## Learning 

ann_diff = FastChain(
  FastDense(2, 20, tanh),
  FastDense(20, 2))

struct kuramoto_ann!
  p::Float32
end

@inline function (km::kuramoto_ann!)(dv, v, edges, p, t)
  # Introducing a buffer for the ann_diff argument will save allocations
  #dv .= ann_diff([dv[1], dv[2], v[1], v[2], km.p], p)
  #dv .= ann_diff([v[1], v[2], km.p], p)
  dv .= ann_diff([v[2], km.p], p) # damit klappt es 
  # We know that the coupling is additive
  for edge in edges
    dv[2] += edge[1]
  end
  nothing
end


annvertices = ODEVertex[]
for p in v_pars
  push!(annvertices, ODEVertex(; f=kuramoto_ann!(p), dim=2, sym=[:θ, :ω]))
end

#annvertex = ODEVertex(; f=kuramoto_ann!, dim=2, sym=[:θ, :ω])
#ann_network = network_dynamics(annvertex, staticedge, g)
ann_network = network_dynamics(annvertices, staticedge, g)

prob_neuralode = ODEProblem(ann_network, x0, tspan, initial_params(ann_diff))

sol_node = solve(prob_neuralode, Tsit5(), saveat=tsteps)
sol_node[2:2:N, :] == Array(sol_node)[2:2:N, :]

plot(Array(sol_node[2:2:N, :])', color=:red, ylims=[-2,2])
plot!(Array(diff_sol[2:2:N, :])', color=:black)


#function predict_neuralode(p)
#  tmp_prob = remake(prob_neuralode, p=p)
#  Array(solve(tmp_prob, Tsit5(), saveat=tsteps,
#  sensealg=SciMLSensitivity.QuadratureAdjoint(autojacvec=SciMLSensitivity.ReverseDiffVJP(true))))
  #sensealg=InterpolatingAdjoint(autojacvec=ReverseDiffVJP(true))))
  #sensealg=autojacvec=SciMLSensitivity.ReverseDiffVJP(true)))
#end

function predict_neuralode(p)
  x = Array(solve(prob_neuralode,Tsit5(),p=p,saveat=tsteps,
  #sensealg=InterpolatingAdjoint(autojacvec=ReverseDiffVJP(true))))
  sensealg=SciMLSensitivity.QuadratureAdjoint(autojacvec=SciMLSensitivity.ReverseDiffVJP(true))))
end  


function loss_neuralode(p)
  x = predict_neuralode(p)
  loss = sum(norm.(x - diff_data))
  return loss
end



## Cache solutions for animation
#sols = [diff_data[2:2:N, :]']

iter = 0
function callback(p, l)
  global iter
  iter += 1
  if iter % 1 == 0
    println(l)
  end
  return false
end
p_ann = zeros(102)
p_ann = initial_params(ann_diff)



adtype = Optimization.AutoZygote()
optf = Optimization.OptimizationFunction((x,p) -> loss_neuralode(x), adtype)
optprob = Optimization.OptimizationProblem(optf, p_ann)
res1 = Optimization.solve(optprob,
                          ADAM(0.005), 
                          callback = callback, 
                          maxiters = 1000)

optprob2 = Optimization.OptimizationProblem(optf, res1.u)
res2 = Optimization.solve(optprob2, ADAM(0.001), callback = callback,maxiters = 500)
opt = res2.u

optprob3 = Optimization.OptimizationProblem(optf, res2.u)
res3 = Optimization.solve(optprob2, ADAM(0.0001), callback = callback,maxiters = 500)
opt = res2.u

