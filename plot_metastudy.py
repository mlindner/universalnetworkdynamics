import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
import os

rc('text', usetex=True)


fig, ax1 = plt.subplots()
plt.cla()

ax1.set_xlabel('steps')
ax1.set_ylabel('loss')
ax1.set_yscale('log')

directory = os.fsencode("meta_studies/")
for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".txt"):
        filepath = os.fsdecode(os.path.join(directory, file))
    else:
        continue
    data = np.loadtxt(filepath, delimiter='\t')
    if data[1] == 60.0:
        ax1.plot(data[3:5002], linewidth=0.5,
                 c='b')  # , label='Learning rate: ' + str(data[0]) + '\n' + \
        # 'chain size: ' + str(data[1]) + '\n' + 'random seed: ' + str(data[2]))
        if data[2] == 1.0:
            ax1.plot(data[3:5002], linewidth=0.5, c='k')
    if data[1] == 30.0:
        ax1.plot(data[3:5002], linewidth=0.5, c='r')
        if data[2] == 1.0:
            ax1.plot(data[3:5002], linewidth=0.5, c='pink')

ax1.legend()
plt.show()
#plt.savefig('plots/metaparam_1.0e-5_number_30_randseed_1.pdf')


fig, ax1 = plt.subplots()
plt.cla()

ax1.set_xlabel('steps')
ax1.set_ylabel('loss')
ax1.set_yscale('log')

directory = os.fsencode("meta_studies/")
for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".txt"):
        filepath = os.fsdecode(os.path.join(directory, file))
    else:
        continue
    data = np.loadtxt(filepath, delimiter='\t')
    if data[1] == 60.0 and data[2] == 2.0:
        ax1.plot(data[3:5002], label='Learning rate: ' + str(data[0]))

ax1.legend()
plt.show()
