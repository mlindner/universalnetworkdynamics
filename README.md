# Project on ANNs in complex networks

### sine.jl
Learn coupling function of a Kuramoto network from data

### node.jl
Learn node dynamics

### node_and_sind.jl
Learn coupling function and node dynamics

### without_ND.jl
Do the above without NetworkDynamics.jl

### metaparam_studies.jl
Study different metaparameter for the ANN. At the time of writing the ReadMe: Learning rate, Random Seed, Network Size
This script should be restructured to use more functions.

### plot_metastudy.py
Plotting in Python for convenience. Could be rewritten in Julia
TODO:
- Save all metaparam experiments in a dataframe for better access
- Improve visualization / labelling of plots

### bug.jl
Weird Enzyme.jl bug. See Github issue.