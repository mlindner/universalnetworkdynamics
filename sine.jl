## Fit coupling of 2nd order Kuramoto model with an ANN


using DiffEqFlux
using NetworkDynamics
using Graphs
using OrdinaryDiffEq
using GalacticOptim
using Random
using Plots

## Defining the graph

Random.seed!(1)

N = 6 # nodes
k = 4 # node degree
g = barabasi_albert(N, k) # graph


## Network dynamics vertex and edge functions
struct kuramoto_inertia!
  p::Float32
end


function (km::kuramoto_inertia!)(dv, v, edges, p, t)
  dv[1] = v[2]
  dv[2] = km.p - v[2]
  for edge in edges
    dv[2] += edge[1]
  end
  return nothing
end

@inline function diffusion_edge!(e, v_s, v_d, p, t)
  e[1] = sin(v_s[1] - v_d[1])
  nothing
end


staticedge = StaticEdge(; f=diffusion_edge!, dim=1)

## generating random values for the parameter value ω_0 of the vertices

v_pars = randn(nv(g))
v_pars .-= sum(v_pars) / nv(g)
odevertices = ODEVertex[]
for p in v_pars
  push!(odevertices, ODEVertex(; f=kuramoto_inertia!(p), dim=2, sym=[:θ, :ω]))
end


kuramoto_network! = network_dynamics(odevertices, staticedge, g)

## Simulation and Plotting

# constructing random initial conditions for nodes (variable θ)
x0 = randn(Float32, 2nv(g))
dx = similar(x0)
datasize = 100 # Number of data points
tspan = (0.0f0, 15.0f0) # Time range
tsteps = range(tspan[1], tspan[2], length=datasize)

diff_prob = ODEProblem(kuramoto_network!, x0, tspan, nothing)
diff_sol = solve(diff_prob, Tsit5(); reltol=1e-6, saveat=tsteps)
diff_data = Array(diff_sol)


plot(diff_sol, vars=2:2:12)

## Learning sine seems to be hard, start with diffusion

ann_diff = FastChain(
  FastDense(2, 20, tanh),
  FastDense(20, 1))

@inline function ann_edge!(e, v_s, v_d, p, t)
  e[1] = ann_diff([v_s[1], v_d[1]], p)[1]
  nothing
end

annedge = StaticEdge(; f=ann_edge!, dim=1, coupling=:antisymmetric)
ann_network = network_dynamics(odevertices, annedge, g)

prob_neuralode = ODEProblem(ann_network, x0, tspan, initial_params(ann_diff))


sol_node = solve(prob_neuralode, Tsit5(), saveat=tsteps)
sol_node[2:2:12, :] == Array(sol_node)[2:2:12, :]

plot(Array(sol_node[2:2:12, :])', color=:red)
plot!(Array(diff_sol[2:2:12, :])', color=:black)


function predict_neuralode(p)
  tmp_prob = remake(prob_neuralode, p=p)
  Array(solve(tmp_prob, Tsit5(), saveat=tsteps))
end

function loss_neuralode(p)
  pred = predict_neuralode(p)
  loss = sum(abs2, diff_data .- pred)
  return loss, pred
end



## Cache solutions for animation
sols = [diff_data[2:2:12, :]']

callback = function (p, l, pred; doplot=true)
  display(l)
  # plot current prediction against data
  # plt = plot(diff_data', label=["data" ""], color=:black)
  # plot!(plt, pred', label=["prediction" ""], color=:red)
  push!(sols, pred[2:2:12, :]')
  if doplot
    #display(plot(plt))
    #frame(anim)
  end
  return false
end

loss_neuralode(initial_params(ann_diff))
callback(initial_params(ann_diff), loss_neuralode(initial_params(ann_diff))...)
initial_params(ann_diff)

result_neuralode = DiffEqFlux.sciml_train(loss_neuralode, prob_neuralode.p, cb=callback)

## Animate

anim = @animate for sol in sols
  plt = plot(tsteps,
    diff_data[2:2:12, :]', label=["data" ["" for i = 1:N-1]...],
    color=:black, ylim=[-2, 2])
  plot!(plt, tsteps, sol, label=["ann fit" ["" for i = 1:N-1]...], color=:red)
end
#gif(anim, "sine_ann.gif", fps=30)



## Check how well the coupling functions is fit 

# (x,y) = (0,y) ↦ sin(-y) = -sin(y)

points = hcat(zeros(1601), collect(-8.0:0.01:8.0))

ann_points = mapslices(x -> ann_diff(x, result_neuralode.u), points, dims=2)

#plot(points[:, 2], label="x")
plot(collect(-8.0:0.01:8.0), -sin.(points[:, 2]), label="data",
  title="coupling function (x,y) -> sin(x-y) at (0,y)")
plot!(collect(-8.0:0.01:8.0), ann_points[:, 1], label="fit")
#png("sine_check.png")


### Plots for Frank
sols
for i = [2, 27, 252]
  plt = plot(tsteps, diff_data[2:2:12, :]', label=["data" ["" for i = 1:N-1]...],
    color=:black, ylim=[-2, 2])
  plot!(plt, tsteps, sols[i], label=["ann fit" ["" for i = 1:N-1]...],
    color=:red, title="Epoch $(i-2)")
  #png("sine_epoch_$(i-2).png")
  display(plt)
end
plt = plot(tsteps, diff_data[2:2:12, :]', label=["data" ["" for i = 1:N-1]...],
  color=:black, ylim=[-2, 2])
plot!(plt, tsteps, sols[end], label=["ann fit" ["" for i = 1:N-1]...],
  color=:red, title="Epoch $(length(sols)) (converged)")
#png("sine_converged.png")



# Plots for Diss
sols
for i = [2, 27, 252]
  plt = plot(tsteps,
    diff_data[2:2:12, :]',
    label=["data" ["" for i = 1:N-1]...],
    color=:black, ylim=[-2.5, 2.5],
    xlabel="t", ylabel="ω", dpi=400,
    tickfont=12, guidefontsize=16, legendfontsize=16
  )
  plot!(plt, tsteps, sols[i], label=["fit" ["" for i = 1:N-1]...],
    color=:red, title="Epoch $(i-2)", legendfontsize=12)
  png("/home/micha/git/Diss/pictures/sine_epoch_$(i-2).png")
  display(plt)
end

begin
  plt = plot(tsteps, diff_data[2:2:12, :]',
    label=["data" ["" for i = 1:N-1]...],
    color=:black, ylim=[-2.5, 2.5],
    xlabel="t", ylabel="ω", dpi=400,
    tickfont=12, guidefontsize=16, legendfontsize=16
  )
  plot!(plt, tsteps, sols[end], label=["fit" ["" for i = 1:N-1]...],
    color=:red, title="Epoch $(length(sols)) (converged)", legendfontsize=12, linestyle=:dash)
  png("/home/micha/git/Diss/pictures/sine_converged.png")

end


## Check how well the coupling functions is fit 
using LaTeXStrings

x = y = range(-1.5pi, stop=1.5pi, length=100)
X = repeat(reshape(x, 1, :), length(y), 1)
Y = repeat(y, 1, length(x))

options = Dict([
  :clims => (-3, 3), 
  :xlabel => L"$\theta_j$", 
  :ylabel => L"$\theta_i$",
  :dpi => 400, 
  :tickfont => 12, 
  :guidefontsize => 16, 
  :legendfontsize => 16,
  :xticks=> ([-1.5pi, -pi, -0.5pi, 0, 0.5pi, pi, 1.5pi],
    [L"$-3/2\pi$", L"\pi", L"-1/2\pi", L"0", L"1/2\pi", L"\pi", L"3/2\pi",]),
  :yticks=> ([-1.5pi, -pi, -0.5pi, 0, 0.5pi, pi, 1.5pi],
    [L"$-3/2\pi$", L"\pi", L"-1/2\pi", L"0", L"1/2\pi", L"\pi", L"3/2\pi",]),
  :colorbar_titlefont => 16, :right_margin => 3Plots.mm,
  :left_margin => 5Plots.mm,
  :yguidefontrotation => -90
  #:colorbar_ticks=>([-3, -2, -1, 0, 1, 2, 3], [L"$-3$", L"-2", L"-1", L"0", L"1", L"2", L"3"])
  ])


heatmap(x, y, map((x, y) -> sin(x - y), X, Y); options..., title=L"$\sin (\theta_j - \theta_i)$"
)
savefig("/home/micha/git/Diss/pictures/sin_heatmap.png")

heatmap(x, y, map((x, y) -> ann_diff([x, y], result_neuralode.u)[1], X, Y); options..., title=L"$g(\theta_j,\theta_i)$"
  )
savefig("/home/micha/git/Diss/pictures/g_heatmap.png")

heatmap(x, y, map((x, y) -> (ann_diff([x, y], result_neuralode.u)[1] - sin(x - y)), X, Y); options..., title=L"$g(\theta_j,\theta_i) - \sin (\theta_j - \theta_i)$"
)
savefig("/home/micha/git/Diss/pictures/diff_heatmap.png")

